import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

// Router
import router from './Examples/router';

// Store
import store from './Examples/store';

const App = () => (
  <Provider store={store}>
    {router}
  </Provider>
);

render(<App/>, document.getElementById('app'));