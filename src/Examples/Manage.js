import React, {Component} from 'react';

// Plugin Components
import TooltipImageManager from '../Component';

export default class ManageTooltips extends Component {
  render() {
    return (
      <div className="container">
        <TooltipImageManager/>
      </div>
    )
  }
}