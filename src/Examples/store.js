import {createStore, combineReducers} from 'redux';

import imageTooltips from '../Component/reducers';

const reducers = combineReducers({
  imageTooltips
});

const store = createStore(reducers);

export default store;

