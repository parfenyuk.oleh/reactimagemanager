import React, {Component} from 'react';
import {connect} from 'react-redux';

// Components
import Image from '../Component/components/Image';
import Tooltips from '../Component/components/Tooltips';

class Main extends Component {
  constructor(props) {
    super(props);

    this.renderTooltips = this.renderTooltips.bind(this);
  }

  renderTooltips() {
    const {imageTooltips} = this.props;
    return imageTooltips.map(image => (
      <div key={image.imageID} className="m-image-tooltips__item">
        <Image image={image}>
          <Tooltips image={image}/>
        </Image>
      </div>
    ))
  }

  render() {
    return (
      <div className="container">
        <h1>Tooltips Image List Example</h1>
        {this.renderTooltips()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    imageTooltips: state.imageTooltips
  }
};

export default connect(mapStateToProps)(Main);