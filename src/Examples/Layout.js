import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Layout extends Component {
  render() {
    return (
      <div>
        <header className="l-header">
          <nav className="navbar navbar-default">
            <div className="container">
              <ul className="nav navbar-nav">
                <li><Link to="/">Main</Link></li>
                <li><Link to="/admin">Manage</Link></li>
              </ul>
            </div>
          </nav>
        </header>
        <div className="container">
          {this.props.children}
        </div>
      </div>
    )
  }
}