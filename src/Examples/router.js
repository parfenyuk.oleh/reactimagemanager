import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'; // // React router v4
import PropTypes from 'prop-types';

// Components
import Admin from './Manage';
import FrontPage from './Main';
import Layout from './Layout';


// Authentication Wrapper for Admin Route 
const AuthRoute = ({loggingIn, component: Component, ...rest}) => (
  <Route {...rest} render={
    (props) => {
      return loggingIn ? (<Component {...props}/>) : (<Redirect to="/"/>);
    }
  }
  />
);

AuthRoute.PropTypes = {
  loggingIn: PropTypes.bool,
  component: PropTypes.func
};

export default (
  <Router>
    <Layout>
      <Switch component={Layout}>
        <Route exact path='/' component={FrontPage}/>
        <AuthRoute exact path='/admin' loggingIn={true} component={Admin}/>
      </Switch>
    </Layout>
  </Router>
)
