import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Compontents
import Tooltip from './Tooltip';


class Tooltips extends Component {
  render() {
    const {tooltips} = this.props.image;

    return (
      <div>
        {
          tooltips.map((tooltip, index) =>
            <Tooltip key={index} {...tooltip}/>
          )
        }
      </div>
    )
  }
}

Tooltips.PropTypes = {
  tooltips: PropTypes.object.isRequired
};

export default Tooltips;
