import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Actions
import {addTooltip} from '../actions';


class Image extends Component {
  constructor() {
    super();
    this.generateNewTooltip = this.generateNewTooltip.bind(this);
  }

  generateNewTooltip(e) {
    const {dispatch} = this.props;
    const {imageID, imageURL, imageAlt} = this.props.image;
    const rect = e.target.getBoundingClientRect();
    const tooltipID = Date.now();
    const clickedPos = (pagePos, targetOffset, targetSize) => {
      return (Math.round((pagePos - targetOffset) / targetSize * 10000) / 100 + "%");
    };
    const position = {
      left: clickedPos(e.pageX, rect.left, e.target.width),
      top: clickedPos(e.pageY, rect.top, e.target.height)
    };

    dispatch(addTooltip({
      tooltipID,
      position,
      desc: ''
    }, imageID))
  }

  renderImage() {
    const {imageURL, imageAlt} = this.props.image;

    return imageURL ?
      <img className="image" src={imageURL} alt={imageAlt} onClick={this.generateNewTooltip}/> :
      <p>Sorry, but link for image was broken.</p>;
  }

  render() {
    return (
      <div className="image-el">
        {this.renderImage()}
        {this.props.children}
      </div>
    )
  }
}

Image.PropTypes = {
  imageURL: PropTypes.string.isRequired,
  imageAlt: PropTypes.string
};

export default Image;


   

