import React, {Component} from 'react';

export default class Modal extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    };

    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  componentWillMount() {
    if (this.props.isOpen) this.setState({
      isOpen: true
    })
  }

  open() {
    this.setState({
      isOpen: true
    })
  }

  close() {
    this.setState({
      isOpen: false
    })
  }

  render() {
    return !this.state.isOpen ? <div/> : (
      <div>
        <div className="modal fade in" tabIndex="-1" role="dialog" style={{display: 'block', overflow: 'auto'}}>
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.close}>
                  <span aria-hidden="true">&times;</span></button>
              </div>
              <div className="modal-body">
                {this.props.children}
              </div>
            </div>
          </div>
        </div>
        <div className="modal-backdrop fade in"></div>
      </div>
    )
  }
}
