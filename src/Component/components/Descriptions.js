import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Actions
import {addTooltip} from '../actions';

// Compontents
import Description from './Description';


class Descriptions extends Component {
  render() {
    const {dispatch, image} = this.props;

    return (
      <div className="tooltip-desc full-width">
        {
          image.tooltips.map((tooltip, index) =>
            <Description key={index} {...tooltip} imageID={image.imageID} dispatch={dispatch}/>
          )
        }
      </div>
    )
  }
}

Descriptions.PropTypes = {
  tooltips: PropTypes.object.isRequired
};

export default Descriptions;
