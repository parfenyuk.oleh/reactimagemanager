import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Tooltip extends Component {
  render() {
    const {position, desc} = this.props;

    return (
      <div className="image-tooltip"
           style={position}
           data-title={desc}>
      </div>
    )
  }
}

Tooltip.PropTypes = {
  position: PropTypes.object.isRequired,
  desc: PropTypes.string
};

export default Tooltip;