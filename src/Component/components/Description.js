import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Actions
import {deleteTooltip, updateTooltipDesc} from '../actions';

class Description extends Component {
  constructor(props) {
    super(props);

    this.state = {
      disabled: 'disabled',
      desc: this.props.desc
    };

    this.onChange = this.onChange.bind(this);
    this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this.remove = this.remove.bind(this);
  }

  onChange(e) {
    this.setState({
      desc: e.target.value
    })
  }

  edit() {
    this.setState({
      disabled: ''
    });
    setTimeout(() => {
      this.descInput.focus()
    }, 0)
  }

  save() {
    const {dispatch, tooltipID, imageID} = this.props;
    this.setState({
      disabled: 'disabled'
    });
    dispatch(updateTooltipDesc(imageID, tooltipID, this.state.desc));
  }

  remove() {
    const {dispatch, tooltipID, imageID} = this.props;

    if (confirm('Are you sure, that you want remove this Tooltip and his Description?')) {
      dispatch(deleteTooltip(imageID, tooltipID));
    }
  }

  renderSaveEdit() {
    return (this.state.disabled === '') ?
      <button className="btn btn-success btn-sm" onClick={this.save}>Save</button> :
      <button className="btn btn-primary btn-sm" onClick={this.edit}>Edit</button>
  }


  render() {
    return (
      <div className="tooltip-description">
        <input
          type="text"
          ref={input => {
            this.descInput = input
          }}
          className="desc-input"
          value={this.state.desc}
          disabled={this.state.disabled}
          onChange={this.onChange}
        />
        <div className="manage-desc">
          {this.renderSaveEdit()}
          <button className="btn btn-danger btn-sm" onClick={this.remove}>Remove</button>
        </div>
      </div>
    )
  }
}

Description.PropTypes = {
  desc: PropTypes.string
};

export default Description;
