import React, {Component} from 'react';

// Components
import imageUploader from './ImageUploader';
import Image from './Image';
import Tooltips from './Tooltips';
import Descriptions from './Descriptions';

// Styles
import './Tooltips.css';

class ImageTooltips extends Component {
  render() {
    return (
      <div>
        <h3>Click on image to add Tooltip</h3>
        <div className="m-image-tooltips__item">
          <Image {...this.props}>
            <Tooltips {...this.props} />
          </Image>
          <Descriptions {...this.props} />
        </div>
      </div>
    )
  }
}

export default ImageTooltips;
