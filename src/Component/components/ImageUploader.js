import React, {Component} from 'react';

//Actions
import {addImage, addTooltip} from '../actions';

// Styles
import './ImageUploader.css';


export default class ImageUploader extends Component {
  constructor(props) {
    super(props);

    this.uploadImage = this.uploadImage.bind(this);
    this.addAlt = this.addAlt.bind(this);
    this.saveImage = this.saveImage.bind(this);

    this.state = {
      imageID: null,
      imageURL: null,
      imageAlt: '',
      tooltips: []
    }
  }

  uploadImage(e) {
    let reader, input, file, imageURL, imageID;

    imageID = Date.now();
    reader = new FileReader();
    input = e.target;
    file = input.files[0];

    reader.onloadend = () => {
      imageURL = reader.result;
      this.setState({imageURL, imageID})
    };

    reader.readAsDataURL(file);
  }

  addAlt(e) {
    const imageAlt = e.target.value;
    this.setState({imageAlt})
  }

  saveImage() {
    const {dispatch, modal} = this.props;
    dispatch(addImage(this.state));

    setTimeout(() => {
      modal.close();
    }, 350)
  }

  renderUploadedImage() {
    if (this.state.imageURL) {
      return (
        <div>
          <img src={this.state.imageURL} alt={this.state.imageAlt}/>
          <hr/>
          <div className="well">
            <label>
              Set alteranative text:
              <input type="text" className="form-control" value={this.state.imageAlt} onChange={this.addAlt}/>
            </label>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-primary" onClick={this.saveImage}>Add image</button>
          </div>
        </div>
      )
    }
  }

  render() {
    return (
      <div className="m-upload-image">
        <form encType="multipart/form-data">
          <input onChange={this.uploadImage} type="file" accept="image/*"/>
        </form>
        {this.renderUploadedImage()}
      </div>
    )
  }
}