export const ADD_IMAGE = "ADD_IMAGE";
export const DELETE_IMAGE = "DELETE_IMAGE";
export const ADD_TOOLTIP = "ADD_TOOLTIP";
export const DELETE_TOOLTIP = "DELETE_TOOLTIP";
export const UPDATE_TOOLTIP_DESC = "UPDATE_TOOLTIP_DESC";

// Action creators
export const addImage = (image) => {
  return {
    type: ADD_IMAGE,
    image
  }
};

export const deleteImage = (imageID) => {
  return {
    type: DELETE_IMAGE,
    imageID
  }
};

export const addTooltip = (tooltip, imageID) => {
  return {
    type: ADD_TOOLTIP,
    tooltip,
    imageID
  }
};

export const deleteTooltip = (imageID, tooltipID) => {
  return {
    type: DELETE_TOOLTIP,
    imageID,
    tooltipID
  }
};

export const updateTooltipDesc = (imageID, tooltipID, desc) => {
  return {
    type: UPDATE_TOOLTIP_DESC,
    imageID,
    tooltipID,
    desc
  }
};