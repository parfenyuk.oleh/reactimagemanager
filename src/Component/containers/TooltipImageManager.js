import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

// Actions
import {deleteImage} from '../actions';

// Components
import Modal from '../components/Modal';
import ImageUploader from '../components/ImageUploader';
import ImageTooltips from '../components/ImageTooltips';


class TooltipImageManager extends Component {
  constructor(props) {
    super(props);

    this.state = {
      imageFiltered: undefined
    };

    this.editChoosenImage = this.editChoosenImage.bind(this);
    this.deleteChoosenImage = this.deleteChoosenImage.bind(this);
  }

  deleteChoosenImage(imageID) {
    const {dispatch} = this.props;
    if (confirm('Are you sure that you want remove image?')) {
      dispatch(deleteImage(imageID));
    }
  }

  editChoosenImage(imageID) {
    const imageFiltered = this.props.imageTooltips.filter((image) => {
      return image.imageID === imageID
    });

    this.setState({
      imageFiltered: imageFiltered[0]
    }, this.refs.modal.open)
  }

  renderImageList() {
    const imageItems = this.props.imageTooltips.map((image, index) => (
      <div className="image-item"
           key={index}>
        <div style={{width: 200}}><img src={image.imageURL} className="img-thumbnail" alt={image.imageAlt}/></div>
        <div className="text-right">
          <button type="button" className="btn btn-danger" onClick={() => {
            this.deleteChoosenImage(image.imageID)
          }}>Delete
          </button>
          &nbsp;
          <button type="button" className="btn btn-primary" onClick={() => {
            this.editChoosenImage(image.imageID)
          }}>Edit
          </button>
        </div>
      </div>
    ));

    return (
      <div className="image-container">
        {imageItems}
      </div>
    )
  }

  renderManaging() {
    return (typeof (this.state.imageFiltered) != 'undefined') ?
      <ImageTooltips image={this.state.imageFiltered} {...this.props} /> :
      <ImageUploader {...this.props} modal={this.refs.modal}/>
  }

  render() {
    return (
      <div className="table-responsive">
        <div className="text-right">
          <button type="button" className="btn btn-primary" onClick={this.editChoosenImage}>Add Image</button>
        </div>
        {this.renderImageList()}
        <Modal ref="modal">
          {this.renderManaging()}
        </Modal>
      </div>
    )
  }
}

TooltipImageManager.PropTypes = {
  imageTooltips: PropTypes.array
};

const mapStateToProps = (state) => {
  return {
    imageTooltips: state.imageTooltips
  }
};

export default connect(mapStateToProps)(TooltipImageManager);