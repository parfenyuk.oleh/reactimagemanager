import {
  ADD_IMAGE,
  DELETE_IMAGE,
  ADD_TOOLTIP,
  DELETE_TOOLTIP,
  UPDATE_TOOLTIP_DESC
} from '../actions/index';

const initialImages = [
  {
    imageID: 1,
    imageURL: 'https://unsplash.it/1140/500',
    imageAlt: 'Image with tooltips',
    tooltips: [
      {
        tooltipID: 0,
        position: {
          left: '50%',
          top: '50%',
        },
        desc: 'Oleh Parfeniuk'
      },
      {
        tooltipID: 1,
        position: {
          left: '10%',
          top: '10%',
        },
        desc: 'Oleh React'
      }
    ]
  }
];


// Helpers
const findImage = (state, id) => {
  var result = {
    isset: false
  };
  state.map((image, index) => {
    if (image.imageID && image.imageID === id) {
      result.isset = true;
      result.pos = index;
    }
  })
  return result;
}

// Reducers
// @Todo - refactor > make composition image/tooltips
const imageTooltips = (state = initialImages, action) => {
  var fimage, imageID, tooltipID, desc;
  switch (action.type) {
    case ADD_IMAGE:
      return [
        ...state,
        action.image
      ]
      break;
    case DELETE_IMAGE:
      ({imageID} = action);
      return state.filter((image) => (
        image.imageID != imageID
      ))
      break;
    case ADD_TOOLTIP:
      const {tooltip} = action;
      fimage = findImage(state, action.imageID);
      if (fimage.isset) {
        return [
          ...state,
          ...state[fimage.pos].tooltips.push(tooltip)
        ]
      } else {
        return state;
      }
      return state;
      break;
    case DELETE_TOOLTIP:
      ({imageID, tooltipID} = action);
      fimage = findImage(state, imageID);
      return state.map(image => {
        if (image.imageID === imageID) {
          image.tooltips = image.tooltips.filter((tooltip) => (
            tooltip.tooltipID != tooltipID
          ));
        }
        return image;
      })
      break;
    case UPDATE_TOOLTIP_DESC:
      ({imageID, tooltipID, desc} = action);
      return state.map(image => {
        if (image.imageID === imageID) {
          image.tooltips = image.tooltips.map((tooltip) => {
            if (tooltip.tooltipID === tooltipID) {
              tooltip.desc = desc;
            }
            return tooltip;
          })
        }
        console.log(image)
        return image;
      })
    default:
      return state;
  }
}

export default imageTooltips;